from django.shortcuts import render
from news.models import Post, Category

def home(request):
    template_name = 'contenido.html'
    noticiaDestacada = Post.objects.filter(prominent=True).order_by('date_published')[:1]
    noticiaDest = noticiaDestacada[0]
    noticias = Post.objects.exclude(pk=noticiaDest.pk).order_by('created_at')[:6]
    data = {'noticias': noticias, 'noticiaDestacada': noticiaDest }

    # select * from blog WHERE status = 1
    # data['posts'] = Post.objects.filter(status=True) 

    return render(request, template_name, data, noticiaDestacada)

def noticia_detalle(request, pk):
    template_name = 'noticia_detalle.html'
    noticia = Post.objects.get(pk=pk)
    data = {'noticia' : noticia}
    # select * from blog WHERE status = 1
    return render(request, template_name, data)








