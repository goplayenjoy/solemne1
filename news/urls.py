from django.urls import path, include
from news import views
from . import views

urlpatterns = [
    path('', views.home, name="home"), # news/
    path('detalle/<int:pk>', views.noticia_detalle, name="noticia_detalle" ), # news/detalle
]
